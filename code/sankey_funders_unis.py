"""
Created on Tue Sep 25 12:28:09 2018

@authors: Noah Weaverdyck, David Hofmann, Tim LaRock

Generates a bipartite network of funders --> universities 
"""

import numpy as np
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap

data = pd.read_table('../data/all_Assistance_Delta_20191011_1.csv', sep=',', header=0)
edumask = (data['recipient_name'].str.contains('UNIV')) | (data['recipient_name'].str.contains('COLLEGE'))
data = data[edumask]

year_mask = data['action_date_fiscal_year'] > 2016
data = data[year_mask]
# remove multiple rows corresponding to several actions of the same award: we are only interested in the total award and
# ignore the individual transactions pertaining to the same award. 
# Pick last occurence -> see section on Tricky aspects of data above.
data.drop_duplicates(subset=['award_id_fain'], inplace=True, keep='last')
# remove non-positive funds since it is unclear how to interpret those.
data = data[data['total_funding_amount'] > 0]

# define minimum columns to be able to perform a simple network analysis:
mincols = ['awarding_agency_name', 'recipient_name', 'federal_action_obligation', 'action_date']

## This is the actual dataset I'll operate on for the graph
all_data = data[mincols]

#plt.figure(figsize=(15,15))

## Generate the graph and compute edge weights
G = nx.DiGraph()
edgelists = dict()
edge_weights = dict()
## Split the data so we can have edgeweights per funder
for agency in all_data.awarding_agency_name.unique():
    agency_data = all_data[all_data.awarding_agency_name == agency]
    edgelists[agency] = []

    ## Get the total weight for current agency across universities
    total_weight = sum([all_data[(all_data.recipient_name == name) & (all_data.awarding_agency_name == agency)].federal_action_obligation.sum() \
                         for name in all_data[all_data.awarding_agency_name == agency].recipient_name.unique()])

    for name in agency_data.recipient_name.unique():
        ## Get the total funds for current university from current agency
        weight = agency_data[agency_data.recipient_name == name].federal_action_obligation.sum()
        ## Filter weights by percentage of total agency funds
        if weight > total_weight*.01:
            G.add_edge(agency, name, weight = weight)
            edgelists[agency].append((agency, name))

    ## Normalize edge weights to be between 0,1 to use consistent color map
    edge_weights[agency] = np.array([data['weight'] for e1,e2, data in G.edges(data=True) if (e1, e2) in edgelists[agency]])
    #edge_weights[agency] = (edge_weights[agency] - edge_weights[agency].min()) / (edge_weights[agency].max() - edge_weights[agency].min())


## Draw edges per agency
link = {'source':list(), 'target':list(), 'value':list()}
labels = list()
idx=0
mapping = dict()
for agency, edgelist in edgelists.items():
    weights = edge_weights[agency]
    labels.append(agency)
    mapping[agency] = idx
    idx+=1
    for i, (_, target) in enumerate(edgelist):
        if target not in labels:
            labels.append(target)
            mapping[target] = idx
            idx+=1

        link['source'].append(mapping[agency])
        link['target'].append(mapping[target])
        link['value'].append(weights[i])

#import plotly.io as pio
#pio.renderers.default = "pdf"
import plotly.graph_objects as go
fig = go.Figure(data=[go.Sankey(
    node = dict(
            pad = 25,
            thickness=40,
      line = dict(color = "black", width = 0.5),
      label = labels,
      color = "blue"
    ),
    link = link)])

fig.update_layout(title_text=r'Agency -> Institution', font_size=7)
#fig.write_image('test.pdf')
fig.show(renderer='browser')
